#pragma once
#include "includes.h"

//coeffs array for Schott formula 
using SchottCoeffs = std::array<mpreal, 6>;

//2D point
struct Point2 {
	mpreal x;
	mpreal y;
};

//2D vector
struct Vector2 {
	Point2 start;
	Point2 dir;//direction

	Vector2 operator+(const Vector2& kV)
	{
		Vector2 res = *this;
		res.dir.x += kV.dir.x;
		res.dir.y += kV.dir.y;
		return res;
	}

	Vector2 operator/(const mpreal& kA)
	{
		Vector2 res = *this;
		res.dir.x /= kA;
		res.dir.y /= kA;
		return res;
	}

	Vector2 operator*(const mpreal& kA)
	{
		Vector2 res = *this;
		res.dir.x *= kA;
		res.dir.y *= kA;
		return res;
	}

};

//beam wavelength is nm
struct Beam {
	Vector2 w_v;//wave vector
	mpreal wave_length;
	mpreal amplitude;
};

//2D second order curve
struct SurfCoeffs {
	//a11*x^2+a22*y^2+2*a12*x*y+2*a13*x+2*a23*y+a33=0
	mpreal a11, a22, a33, a12, a13, a23;
};

//surface type
struct SurfType {
	enum class Characteristic
	{
		Common, Mirror, Ending
	};
	Characteristic kind_of_surface;
	SchottCoeffs schott_coeffs;
};

//surface
struct Surface {
	SurfType surf_type;
	Vector2 surf_speed;
	SurfCoeffs surf_coeffs;
};

//log info
struct LogInfo {
	Beam beam;
	mpreal time;
	mpreal refractive_index;
};

//params of uniform grid
struct GridParams {
	enum class Characteristic
	{
		Arsec, Meter
	};
	Characteristic grid_type;
	Vector2 center_speed;
	mpreal cell_size;
	mpreal left_border;
	const int KScatterSize;
};

struct Scatter {
	std::vector<mpreal> amplitudes;
	std::vector<mpreal> phases;
};