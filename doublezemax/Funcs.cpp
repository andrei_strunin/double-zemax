#include "funcs.h"
#include "settings.h"

bool if_story = false;
bool if_last_x = true;

std::string& trim_right(std::string& s, const std::string& kTrimmedChars)
{
	if (s.length() == 0)
		return s;
	return s.erase(s.find_last_not_of(kTrimmedChars) + 1);
}

std::string& trim_left(std::string& s, const std::string& kTrimmedChars)
{
	if (s.length() == 0)
		return s;
	return s.erase(0, s.find_first_not_of(kTrimmedChars));
}

std::string& trim(std::string& s, const std::string& kTrimmedChars)
{
	if (s.length() == 0)
		return s;
	return trim_left(trim_right(s, kTrimmedChars), kTrimmedChars);
}

int _sign(const mpreal& kA) {
	if (kA >= 0)
		return 1;
	else
		return -1;
}

mpreal Gauss(const mpreal& kSigma, const mpreal& kX, const mpreal& kD)
{
	return exp(-pow(kX - kD, 2) / 2 / pow(kSigma, 2));
	// / sigma / mpfr::pow(2 * M_PI, 2);
}

mpreal Distance(const Point2& kM, const mpreal& kA, const mpreal& kB,
	const mpreal& kC)
{
	return mpreal(abs(kA * kM.x + kB * kM.y + kC) / sqrt(kA * kA + kB * kB));
}

//////////////////////////////////VECTOR MATH///////////////////////////////////

mpreal module(const Vector2& kV)
{
	return sqrt(pow(kV.dir.x, 2) + pow(kV.dir.y, 2));
}

Vector2 normalize(const Vector2& kV)
{
	Vector2 res;
	res.dir.x = kV.dir.x / module(kV);
	res.dir.y = kV.dir.y / module(kV);
	res.start = kV.start;

	return res;
}

Vector2 perpendiculize(const Vector2& kV)
{
	Vector2 res;
	res.start = kV.start;
	if (kV.dir.y != 0)
	{
		res.dir.x = 1;
		res.dir.y = -kV.dir.x / kV.dir.y;
		return normalize(res);
	}
	else
		if (kV.dir.x != 0)
		{
			res.dir.y = 1;
			res.dir.x = -kV.dir.y / kV.dir.x;
			return normalize(res);
		}
		else
		{
			res.dir.x = 0;
			res.dir.y = 0;
			return res;
		}
}

mpreal Dot(const Vector2& kV1, const Vector2& kV2)
{
	return kV1.dir.x * kV2.dir.x + kV1.dir.y * kV2.dir.y;
}

mpreal SignSin(const Vector2& kV)
{
	Vector2 d = normalize(kV);
	Vector2 h;
	h.dir.x = 1; h.dir.y = 0;
	return _sign(d.dir.y) * sqrt(1 - pow(Dot(d, h), 2));
}

////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////SURFACE MATH//////////////////////////////////

Vector2 NormalToSurface(const SurfCoeffs& kSurfCoeffs, const Point2& kP)
{
	Vector2 res;
	res.start = kP;
	res.dir.x = kSurfCoeffs.a11 * kP.x + kSurfCoeffs.a12 * kP.y
		+ kSurfCoeffs.a13;
	res.dir.y = kSurfCoeffs.a12 * kP.x + kSurfCoeffs.a22 * kP.y
		+ kSurfCoeffs.a23;
	return normalize(res);
}

mpreal SchottFormula(const SchottCoeffs& kA, const mpreal& kWL)
{
	mpreal w_l = kWL * pow(10, 6);
	//as i defined vacuum with zeros in schottcoeffs zero in A[0] means vacuum 
	if (kA[0] == 0)
		return 1;
	else
		return sqrt(kA[0] + kA[1] * pow(w_l, 2) + (kA[2] / pow(w_l, 2))
			+ (kA[3] / pow(w_l, 4)) + (kA[4] / pow(w_l, 6))
			+ (kA[5] / pow(w_l, 8)));
}

SurfCoeffs TimeRec(const SurfCoeffs& kS, const Vector2& kV, const mpreal& kT)
{
	SurfCoeffs res;
	res.a11 = kS.a11;
	res.a22 = kS.a22;
	res.a12 = kS.a12;
	res.a13 = kS.a13 + kS.a11 * kV.dir.x * kT + kS.a12 * kV.dir.y * kT;
	res.a23 = kS.a23 + kS.a22 * kV.dir.y * kT + kS.a12 * kV.dir.x * kT;
	res.a33 = kS.a33 + kS.a11 * pow(kV.dir.x * kT, 2)
		+ kS.a22 * pow(kV.dir.y * kT, 2)
		+ 2 * kS.a12 * kV.dir.x * kT * kV.dir.y * kT
		+ 2 * kS.a13 * kV.dir.x * kT + 2 * kS.a23 * kV.dir.y * kT;

	return res;
}

std::tuple<Point2, mpreal> TimeAndPointOfCross(const SurfCoeffs& kSurfCoeffs,
	const Beam& kBeam, const Vector2& kSurfSpeed, const mpreal& kN)
{
	mpreal a, b, c, c_speed, sin_phi, cos_phi, t1, t2, time, new_x, new_y;
	Point2 p;
	Vector2 h, beam_dir = normalize(kBeam.w_v);
	h.dir.x = 1; h.dir.y = 0;
	c_speed = SPEED_OF_LIGHT;
	mpreal const& kVx = kSurfSpeed.dir.x;
	mpreal const& kVy = kSurfSpeed.dir.y;
	auto& an = kSurfCoeffs;
	auto& cx = c_speed * Dot(beam_dir, h) / kN,
		& cy = c_speed * SignSin(beam_dir) / kN, & l = kBeam.w_v.start.x,
		& m = kBeam.w_v.start.y;

	//coeffs of square equation relative to t 
	a = an.a11 * cx * cx + 2 * an.a12 * cx * cy + an.a22 * cy * cy 
		+ an.a11 * cx * kVx + an.a12 * cy * kVx + an.a11 * kVx * kVx
		+ 2 * an.a12 * cx * kVy + 2 * an.a22 * cy * kVy + 2 * an.a12 * kVx * kVy
		+ an.a22 * kVy * kVy;
	b = 2 * an.a13 * cx + 2 * an.a23 * cy + 2 * an.a11 * cx * l
		+ 2 * an.a12 * cy * l + 2 * an.a12 * cx * m
		+ 2 * an.a22 * cy * m + 2 * an.a13 * kVx + 2 * an.a11 * l * kVx
		+ 2 * an.a12 * m * kVx + 2 * an.a23 * kVy
		+ 2 * an.a12 * l * kVy + 2 * an.a22 * m * kVy;
	c = an.a33 + 2 * an.a13 * l + an.a11 * l * l + 2 * an.a23 * m
		+ 2 * an.a12 * l * m + an.a22 * m * m;

	//if plane
	if (a == 0)
	{
		time = -c / b;
	}
	else
		//solution of sq eq
	{
		t1 = (-b + sqrt(b * b - 4 * a * c)) / 2 / a;
		t2 = (-b - sqrt(b * b - 4 * a * c)) / 2 / a;

		if (t1 <= 0)
		{
			if (t2 >= 0)
				time = t2;
			else
				time = 401;
		}
		else
		{
			if (t2 <= 0 || t2 < pow(10, -15))
				time = t1;
			else
				time = (t1 < t2) ? t1 : t2;
		}
	}

	//point of cross
	new_x = kBeam.w_v.start.x + cx * time;
	new_y = kBeam.w_v.start.y + cy * time;
	p.x = new_x; p.y = new_y;

	return std::make_tuple(p, time);
}

//todo previous speed
Beam Refraction(const SurfCoeffs& kSurfCoeffs, const Beam& kBeam,
	const Vector2& kSurfSpeed, const Point2& kPointOfCross,
	const mpreal& kCurrentRIndex, const mpreal& kN, int& kSignQ)
{
	Beam new_beam;
	mpreal c = SPEED_OF_LIGHT;
	mpreal w = 2 * M_PI * c / kBeam.wave_length;

	Vector2
		s_speed = kSurfSpeed, //surfSpeed is const, overload won't work
		normal = NormalToSurface(kSurfCoeffs, kPointOfCross),
		tangent = perpendiculize(normal),
		k = kBeam.w_v;
	normal = normal * (mpreal)_sign(k.dir.y * normal.dir.y);
	tangent = tangent * (mpreal)_sign(k.dir.x * tangent.dir.x);

	Vector2
		beta = s_speed / c,
		beta_n = normal * Dot(beta, normal),
		beta_t = tangent * Dot(beta, tangent),
		kn = normal * Dot(k, normal),
		kt = tangent * Dot(k, tangent),
		un = normal * Dot(s_speed, normal),
		ut = tangent * Dot(s_speed, tangent),
		d = kt * c / (Dot(k, un) - w);

	//solution of disperse sq eq
	mpreal
		invariant = Dot(k, kSurfSpeed) - w,
		kappa = pow(kN, 2) - 1,
		gamma = 1 / (1 - pow(module(beta), 2)),
		q = ((1 + kappa * pow(gamma, 2) * (1 - pow(module(beta_n), 2)))
			- pow(module(d), 2) * ((1 - pow(module(beta), 2))
			- kappa * pow(gamma, 2) * pow((module(beta) - module(beta_n)), 2))
			+ kappa * pow(gamma, 2) * Dot(d, beta_t)
			* (2 * (1 - module(beta) * module(beta_n))
			+ (1 - pow(module(beta), 2)) * Dot(d, beta_t))),
		kn_new = (w - Dot(k, un)) / c * ((module(beta) + kappa * pow(gamma, 2)
			* (module(beta) - module(beta_n)) * (1 + Dot(beta_t, d)))
			+ kSignQ * sqrt(q)) / ((1 - pow(module(beta), 2))
			- kappa * pow(gamma, 2) * pow((module(beta) - module(beta_n)), 2));

	k = normal * kn_new + kt;

	new_beam.w_v.dir = k.dir;  new_beam.wave_length = 2 * M_PI * c
		/ (-invariant + Dot(k, kSurfSpeed));
	new_beam.w_v.start = kBeam.w_v.start; new_beam.amplitude = kBeam.amplitude;

	return new_beam;
}

std::tuple<mpreal, mpreal> Interference(const mpreal& kA1, const mpreal& kPhi1,
	const mpreal& kA2, const mpreal kPhi2)
{
	auto& a = sqrt(pow(kA1, 2) + pow(kA2, 2)
		+ 2 * kA1 * kA2 * cos((kPhi2 - kPhi1)));// * M_PI / 180
	auto& phi = atan((kA1 * sin(kPhi1) + kA2 * sin(kPhi2)) /
		(kA1 * cos(kPhi1) + kA2 * cos(kPhi2)));
	return std::make_tuple(a, phi);
}


LogInfo BeamPassing(const Beam& kBeam, const Surface& kSurface,
	const mpreal& kCurrentTime, const mpreal& kCurrentRIndex)
{
	int sign_q;

	//new surf coeffs at the moment
	SurfCoeffs surf_coeffs = TimeRec(kSurface.surf_coeffs, kSurface.surf_speed,
		kCurrentTime);

	//refractive index
	mpreal n = SchottFormula(kSurface.surf_type.schott_coeffs,
		kBeam.wave_length);
	Beam new_beam;
	LogInfo data;

	// obtaining of time and point of crossing surface by a beam 
	std::tuple<Point2, mpreal> time_point = TimeAndPointOfCross(surf_coeffs,
		kBeam, kSurface.surf_speed, kCurrentRIndex);

	// choose Q sign
	switch (kSurface.surf_type.kind_of_surface)
	{
	case SurfType::Characteristic::Common:
	{
		sign_q = 1;

		// obtaining new wave vector
		new_beam = Refraction(surf_coeffs, kBeam, kSurface.surf_speed,
			std::get<0>(time_point), kCurrentRIndex, n, sign_q);
		new_beam.w_v.start = std::get<0>(time_point);
		data.beam = new_beam; data.refractive_index = n;
		data.time = std::get<1>(time_point);
		break;
	}
	case SurfType::Characteristic::Mirror:
	{
		sign_q = -1;

		new_beam = Refraction(surf_coeffs, kBeam, kSurface.surf_speed,
			std::get<0>(time_point), kCurrentRIndex, n, sign_q);
		new_beam.w_v.start = std::get<0>(time_point);
		data.beam = new_beam; data.refractive_index = n;
		data.time = std::get<1>(time_point);
		break;
	}
	//here we get only newBeam start point aka beam point on surface
	case SurfType::Characteristic::Ending:
	{
		new_beam.w_v.start = std::get<0>(time_point);
		new_beam.amplitude = kBeam.amplitude;
		new_beam.wave_length = kBeam.wave_length;
		data.beam = new_beam; data.time = std::get<1>(time_point);
		break;
	}
	}

	return data;
}

///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
///MODELING PROCEDURES

void DataPrint(const LogInfo& kData, std::fstream& kF, const int kI,
	const int kJ, const SurfType::Characteristic& surfType)
{
	std::cout.setf(std::ios::fixed); std::cout.precision(10);
	//For the first beam prints header	
	if (kJ == -1)
		kF << "beam number " << kI + 1 << std::endl << "	" << "x0" << "	"
		<< "y0" << "	" << "x" << "	" << "y" << "	" << "lambda" << "	"
		<< "k" << "	" << "n" << "	" << "t" << std::endl;

	if (surfType == SurfType::Characteristic::Ending)
	{
		kF << "final" << "	";
		kF << "xf" << "	" << "yf" << "	" << "tf" << std::endl;
		kF << "	" << kData.beam.w_v.start.x << "	" << kData.beam.w_v.start.y
		<< "	" << kData.time << std::endl << std::endl;
	}
	else
	{
		kF << kJ + 1 << "	" << kData.beam.w_v.start.x << "	"
			<< kData.beam.w_v.start.y << "	"
			<< kData.beam.w_v.dir.x / module(kData.beam.w_v) << "	" 
			<< kData.beam.w_v.dir.y / module(kData.beam.w_v) << "	"
			<< kData.beam.wave_length << "	" 
			<< module(kData.beam.w_v) << "	"
			<< kData.refractive_index << "	" << kData.time << std::endl;
	}
}

void BeamThreading(int thread_id, int start, int len,
	const std::vector<Beam>& kBeams, const std::vector<Surface>& kSurfaces,
	const GridParams& kGrid, mpreal& buffer_t, Scatter& scatter,
	ProgressPrinter& progress_printer)
{
	auto start_time = std::chrono::high_resolution_clock::now();
	mpreal::set_default_prec(128);

	std::fstream last_coord_f, single_story_f;
	last_coord_f.open("last_x" + std::to_string(thread_id) + ".txt",
		std::fstream::in | std::fstream::out | std::fstream::trunc);
	last_coord_f.setf(std::ios::fixed); last_coord_f.precision(20);
	single_story_f.open("story" + std::to_string(thread_id) + ".txt",
		std::fstream::in | std::fstream::out | std::fstream::trunc);
	single_story_f.setf(std::ios::fixed); single_story_f.precision(20);

	scatter.amplitudes = std::vector<mpreal>(kGrid.KScatterSize, 0);
	scatter.phases = std::vector<mpreal>(kGrid.KScatterSize, 0);

	for (size_t i = start; i < start + len; i++)
	{
		LogInfo data = LogInfo();
		Beam new_beam = kBeams[i];
		mpreal
			phase = 0,//beam phase
			ki_ri = 0,// part of phi = kr-wt
			wi_ti = 0,
			current_path = 0,
			current_time = 0,
			current_index = 1,//ray starts in vacuum
			first_x = new_beam.w_v.start.x,
			c = SPEED_OF_LIGHT;
		int cell;
		int beam_quantity = kBeams.size();

		data.beam = new_beam; data.time = current_time;
		data.refractive_index = current_index;

		if (if_story)
			DataPrint(data, single_story_f, i, -1,
				SurfType::Characteristic::Common);

		//surf cycle
		for (int j = 0; j < kSurfaces.size(); j++)
		{
			data = BeamPassing(new_beam, kSurfaces[j],
				current_time, current_index);

			ki_ri += module(new_beam.w_v) 
				* sqrt(pow(data.beam.w_v.start.x - new_beam.w_v.start.x, 2)
				+ pow(data.beam.w_v.start.y - new_beam.w_v.start.y, 2));
			wi_ti += 2 * M_PI * c / new_beam.wave_length * data.time;

			if (if_story)
				DataPrint(data, single_story_f, i, j,
					kSurfaces[j].surf_type.kind_of_surface);

			current_path += sqrt(pow(data.beam.w_v.start.x
				- new_beam.w_v.start.x, 2) + pow(data.beam.w_v.start.y
					- new_beam.w_v.start.y, 2));

			current_time += data.time;
			current_index = data.refractive_index;

			new_beam = data.beam;
		}

		// Phase = kr + w*dT
		phase = ki_ri;
		// +2 * M_PI * c / newBeam.waveLength * (currentTime - bufferT);

		//choosing cell on uniform grid
		switch (kGrid.grid_type) 
		{
		case GridParams::Characteristic::Arsec:
		{
			mpreal center_x = current_time * kGrid.center_speed.dir.x
				+ kGrid.center_speed.start.x;
			mpreal center_y = current_time * kGrid.center_speed.dir.y
				+ kGrid.center_speed.start.y;
			mpreal final_x = new_beam.w_v.start.x;
			mpreal final_y = new_beam.w_v.start.y;
			Vector2 cf = { {0}, {final_x - center_x, final_y - center_y} };
			mpreal dista = Distance({ center_x, center_y },
				2 * kSurfaces[kSurfaces.size() - 1].surf_coeffs.a13,
				2 * kSurfaces[kSurfaces.size() - 1].surf_coeffs.a23,
				kSurfaces[kSurfaces.size() - 1].surf_coeffs.a33);
			mpreal moda = module(cf);
			mpreal theta = _sign(final_x - center_x) * acos(dista / moda)
				* 180 / M_PI * 3600;
			cell = int((theta - kGrid.left_border) / kGrid.cell_size);
			break;
		}
		case GridParams::Characteristic::Meter:
		{
			cell = int((new_beam.w_v.start.x - kGrid.left_border)
				/ kGrid.cell_size);
			break;
		}
		}

		if (if_last_x)
			last_coord_f << first_x << "	" << new_beam.w_v.start.x << "	" 
			<< new_beam.amplitude << "	" << phase <<"	"
			<< scatter.phases[cell] - phase << std::endl;
		/*
		<<module(newBeam.wV)<<"	"<< 2 * M_PI * c/newBeam.waveLength<<"	"
		<<currentTime<<"	"<<currentPath<< std::endl;*/

		//interferention in cells
		std::tuple<mpreal,mpreal> aPhi = Interference(scatter.amplitudes[cell],
			scatter.phases[cell], new_beam.amplitude, phase);
		scatter.amplitudes[cell] = std::get<0>(aPhi);
		scatter.phases[cell] = std::get<1>(aPhi);
		
		//printing progress
		if (global_settings.show_progress && len > 100)
			if ((i - start) % (len / 100) == 0)
				progress_printer.ReportProgress((i - start) / (float)len);
	}
	if (global_settings.show_progress)
		progress_printer.ReportProgress(1.0f);
	last_coord_f.close();
	single_story_f.close();
	mpfr_free_cache();

	auto end_time = std::chrono::high_resolution_clock::now();
	auto duration = end_time - start_time;
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(duration);
	printf("Thread #%d finished in %ldms\n", thread_id, ms.count());
}

void Modeling(const std::vector<Beam>& kBeams,
	const std::vector<Surface>& kSurfaces, const GridParams& kGrid)
{
	unsigned int threads_count = global_settings.threads == 0
		? std::thread::hardware_concurrency() : global_settings.threads;

	std::fstream interference_f;
	interference_f.open("interference.txt",
		std::fstream::in | std::fstream::out | std::fstream::trunc);
	interference_f.setf(std::ios::fixed); interference_f.precision(20);

	mpreal c = SPEED_OF_LIGHT;
	mpreal buffer_t = 0;
	std::vector<Scatter> scatters(threads_count, Scatter());
	Scatter scatter;
	scatter.amplitudes = std::vector<mpreal>(kGrid.KScatterSize, 0);
	scatter.phases = std::vector<mpreal>(kGrid.KScatterSize, 0);

	//parallel call
	std::vector<std::thread> calc_threads = {};
	std::vector<ProgressPrinter> progress_printers;
	progress_printers.reserve(threads_count);

	int progress_output_line_offset = 1;
#ifdef USE_WINAPI_CONSOLE
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	progress_output_line_offset = csbi.dwCursorPosition.Y;
#endif

	for (size_t i = 0; i < threads_count; i++)
	{
		progress_printers.push_back(ProgressPrinter(progress_output_line_offset
			+ i));
		int len = kBeams.size() / threads_count;
		if (i != threads_count - 1)
			calc_threads.push_back(std::thread(BeamThreading, i, i * len, len,
				kBeams, kSurfaces, kGrid, std::ref(buffer_t),
				std::ref(scatters[i]), std::ref(progress_printers[i])));
		else
			calc_threads.push_back(std::thread(BeamThreading, i, i * len,
				len + kBeams.size() % threads_count, kBeams, kSurfaces, kGrid,
				std::ref(buffer_t), std::ref(scatters[i]),
				std::ref(progress_printers[i])));
	}
	for (size_t i = 0; i < threads_count; i++)
	{
		calc_threads[i].join();		
	}

	//merging scatters
	for (int i = 0; i < threads_count; i++)
	{
		for (int j = 0; j < scatter.amplitudes.size(); j++)
		{
			auto& a1 = scatter.amplitudes[j];
			auto& phi1 = scatter.phases[j];
			auto& a2 = scatters[i].amplitudes[j];
			auto& phi2 = scatters[i].phases[j];

			//interferention in cells
			if (a1 == 0 && a2 == 0 && phi1 == 0 && phi2 == 0)
			{
				scatter.amplitudes[j] = 0;
				scatter.phases[j] = 0;
			}
			else
			{
				std::tuple<mpreal,mpreal> aPhi = Interference(a1, phi1, a2,
					phi2);
				scatter.amplitudes[j] = std::get<0>(aPhi);
				scatter.phases[j] = std::get<1>(aPhi);
			}
		}
	}

	std::cout << "trajectories done" << std::endl;

	//prints intensities
	interference_f << "x" << "	" << "I" << std::endl;
	for (int i = 0; i < scatter.amplitudes.size(); i++)
	{
		interference_f << kGrid.cell_size * mpreal(i) + kGrid.left_border 
			<< "	" << pow(scatter.amplitudes[i], 2) << std::endl;
		if (i % (scatter.amplitudes.size() / 100) == 0)
			std::cout << i * 100 / (scatter.amplitudes.size()) << "%" << "\r";
	}
	std::cout << "interference done" << std::endl;
	interference_f.close();

	//merging files
	std::ofstream out_last_x("last_x.txt",
		std::ios_base::out | std::ios_base::trunc);
	std::ofstream out_story("story.txt",
		std::ios_base::out | std::ios_base::trunc);
	out_last_x << "firstX" << "	" << "lastX" << "	" << "Amp" << "	" << "phase"
		<<"	"<<"dphase"<< std::endl;
	for (int i = 0; i < threads_count; i++)
	{
		std::ifstream in_last_x("last_x" + std::to_string(i) + ".txt");
		std::ifstream in_story("story" + std::to_string(i) + ".txt");
		for (std::string str; std::getline(in_last_x, str); )
		{
			out_last_x << str << std::endl;
		}
		in_last_x.close();
		fs::remove("last_x" + std::to_string(i) + ".txt");
		for (std::string str; std::getline(in_story, str); )
		{
			out_story << str << std::endl;
		}
		in_story.close();
		fs::remove("story" + std::to_string(i) + ".txt");
	}
	out_last_x.close();
	out_story.close();
}

///////////////////////////////////////////////////////////////////
