#pragma once
#include "structs.h"
#include "progressprinter.h"

#define TRIMMED_CHARS "\t\n\r "

std::string& trim_right(std::string& s,
    const std::string& kTrimmedChars = TRIMMED_CHARS);

std::string& trim_left(std::string& s,
    const std::string& kTrimmedChars = TRIMMED_CHARS);

std::string& trim(std::string& s,
    const std::string& kTrimmedChars = TRIMMED_CHARS);

//returns 1 if a>=0 else -1
int _sign(const mpreal& kA);

//irregular gauss distribution(gauss(d)=1)
mpreal Gauss(const mpreal& kSigma, const mpreal& kX, const mpreal& kD);

//distance between dot and line a*x+b*y+c=0
mpreal Distance(const Point2& kM, const mpreal& kA, const mpreal& kB,
    const mpreal& kC);

//////////////////////////////////VECTOR MATH///////////////////////////////////

//returns normalized vector
Vector2 normalize(const Vector2& kV);

//returns perpendiculized vector
Vector2 perpendiculize(const Vector2& kV);

//returns vector module
mpreal module(const Vector2& kV);

//returns dotproduct of 2 vectors
mpreal Dot(const Vector2& kV1, const Vector2& kV2);

//returns right sin of angle between vector and horizontale
mpreal SignSin(const Vector2& kV);

////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////SURFACE MATH//////////////////////////////////

//returns normale to surface at point
Vector2 NormalToSurface(const SurfCoeffs& kSurfCoeffs, const Point2& kP);

//Schott formula (wavelength in nm)
mpreal SchottFormula(const SchottCoeffs& kA, const mpreal& kWL);

//surf coeffs rerecording with time (coeffs, speed, time)
SurfCoeffs TimeRec(const SurfCoeffs& kS, const Vector2& kV, const mpreal& kT);

//point and time of crossing
std::tuple<Point2, mpreal> TimeAndPointOfCross(const SurfCoeffs& kSurfCoeffs,
    const Beam& kB, const Vector2& kSurfSpeed, const mpreal& kN);

//returns new beam after refraction/reflection
Beam Refraction(const SurfCoeffs& kSurfCoeffs, const Beam& Beam,
    const Vector2& kSurfSpeed, const Point2& kPointOfCross,
    const mpreal& kCurrentN, const mpreal& kN, int& kSingQ);

//returns new amplitude and phase of 2 interfering beams
std::tuple<mpreal, mpreal> Interference(const mpreal& kA1, const mpreal& kPhi1,
    const mpreal& kA2, const mpreal kPhi2);

//returns new beam + log info after refraction/reflection on surface
LogInfo BeamPassing(const Beam& kB, const Surface& kS,
    const mpreal& kCurrentTime, const mpreal& kCurrentRIndex);

////////////////////////////////////////////////////////////////////////////////

//////////////////////////////MODELING PROCEDURES///////////////////////////////

//data printing
void DataPrint(const LogInfo& kData, const std::fstream& kF, const int kI,
    const int kJ, const SurfType::Characteristic& kSurfType);

//returns scatter
void BeamThreading(int thread_id, int start, int len,
    const std::vector<Beam>& kBeams, const std::vector<Surface>& kSurfaces,
    const GridParams& kGrid, mpreal& kBufferT, Scatter& kScatter,
    ProgressPrinter& kProgress_printer);

//main procedure that works with arrays of beams ad surfaces
void Modeling(const std::vector<Beam>& kBeams,
    const std::vector<Surface>& kSurfaces, const GridParams& kGrid);

////////////////////////////////////////////////////////////////////////////////