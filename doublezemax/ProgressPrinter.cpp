#include "progressprinter.h"
#include "settings.h"

#ifndef USE_WINAPI_CONSOLE
	#include <sys/ioctl.h>
	#include <stdio.h>
	#include <unistd.h>
#endif

ProgressPrinter::ProgressPrinter(int line_number)
{
	if (!global_settings.show_progress)
		return;
#ifdef USE_WINAPI_CONSOLE
	console_output_handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(console_output_handle, &csbi);
	terminal_width = csbi.dwSize.X;	
#else
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	terminal_width = w.ws_col;
#endif

	this->line_number = line_number;
	progress_line_buffer = std::vector<char>(terminal_width + 1, ' ');
	progress_line_buffer[0] = '[';
	progress_line_buffer[terminal_width - 1] = ']';
	progress_line_buffer[terminal_width] = '\0';
	std::cout << std::endl; // Reserve space for line
}

void ProgressPrinter::ReportProgress(float progress) 
{
	if (!global_settings.show_progress)
		return;
	int progress_degree = (int)((terminal_width - 2) * progress);
	for (size_t i = 0; i < progress_degree; i++)
	{
		progress_line_buffer[i + 1] = '=';
	}

#ifdef USE_WINAPI_CONSOLE    
	DWORD bytes_written;
	WriteConsoleOutputCharacterA(console_output_handle,
		progress_line_buffer.data(), progress_line_buffer.size() - 1,
		COORD{ 0, (SHORT)line_number }, &bytes_written);
#else
	printf("\33[s\33[%d;0H%s\33[u", line_number, progress_line_buffer.data());
#endif
}
