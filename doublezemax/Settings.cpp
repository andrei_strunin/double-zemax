#include "settings.h"

Settings global_settings;

void Settings::Add(const std::string& key, const std::string value)
{
	try {
		UpdatersMap& map = updaters_map;
		auto search = map.find(key);
		if (search != map.end())
			search->second(value); // ->second points to updater func
		else 
			printf("Warning! Parser for setting key '%s' not found\n",
				key.c_str());
	}
	catch (...) {
		printf("ERROR! Can't parse setting '%s' with value '%s'\n", key.c_str(),
			value.c_str());
	}
}

void Settings::PrintAllSettings()
{
	PRINTVALUEOF(beam_quantity);
	PRINTVALUEOF(orbit_height);
	PRINTVALUEOF(speed);
	PRINTVALUEOF(cell_size);
	PRINTVALUEOF(left_border);
	PRINTVALUEOF(scatter_size);
	PRINTVALUEOF(show_progress);
	PRINTVALUEOF(settings_file);
	PRINTVALUEOF(threads);
}

Settings::Settings() {}

void Settings::AddFromCommandLine(int argc, char* argv[])
{
	for (size_t i = 1; i < argc; i++)
	{
		char* arg = argv[i];

		std::string key_value(arg);
		size_t delimiter_index = key_value.find("=");
		if (delimiter_index == std::string::npos)
		{
			printf("ERROR! Command line arg '%s' do not contain"
				"'=' sign. Skipping...\n", key_value.c_str());
			continue;
		}
		std::string key = key_value.substr(0, delimiter_index);
		std::string value = key_value.substr(delimiter_index + 1);

		Add(key, value);
	}
}

void Settings::AddFromFile(const std::string& path) 
{
	std::ifstream f(path);
	if (f.fail()) {
		printf("ERROR! Can't open file '%s'. Skipping...\n", path.c_str());
		return;
	}
	std::string line;
	int line_n = 0;
	while (std::getline(f, line))
	{
		line_n++;
		size_t comment_index = line.find('#');
		if (comment_index != std::string::npos) 
			line = line.substr(0, comment_index);
		line = trim(line);
		if (line.empty())
			continue;
		size_t delimiter_index = line.find("=");
		if (delimiter_index == std::string::npos)
		{
			printf("Line #%d of file '%s' do not contain '=' sign."
				" Skipping... (%s)\n", line_n, path.c_str(), line.c_str());
			continue;
		}
		std::string key = line.substr(0, delimiter_index);
		std::string value = line.substr(delimiter_index + 1);
		trim(key);
		trim(value);
		if (key == "settings-file") 
		{
			printf("Warning! Settings file can't contain 'settings-file'"
				"setting! Skipping...\n");
			continue;
		}
		Add(key, value);
	}
}

template<typename T>
T Settings::Hook(const std::string& name, T default_value, UpdaterFunc f)
{
	updaters_map[name] = f;
	return default_value;
}

size_t Settings::ParseLong(const std::string& value)
{
	size_t pow_sign_index = value.find('^');
	if (pow_sign_index != std::string::npos)
		return (size_t)std::pow(stoull(value.substr(0, pow_sign_index)),
			stoull(value.substr(pow_sign_index + 1)));
	return stoull(value);
}

mpreal Settings::ParseMpreal(const std::string& value)
{
	size_t pow_sign_index = value.find('^');
	if (pow_sign_index != std::string::npos)
		return (mpreal)mpfr::pow(mpreal(value.substr(0, pow_sign_index)),
			mpreal(value.substr(pow_sign_index + 1)));
	return mpreal(value);
}

int Settings::ParseInt(const std::string& value)
{
	return std::stoi(value);
}

bool Settings::ParseBool(const std::string& value)
{
	return value == "true" || value == "True";
}
