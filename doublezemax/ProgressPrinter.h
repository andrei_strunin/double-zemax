#pragma once
#include "includes.h"

class ProgressPrinter
{
private:
	int line_number;
	int terminal_width;
	std::vector<char> progress_line_buffer;
#ifdef USE_WINAPI_CONSOLE
	HANDLE console_output_handle;
#endif
public:
	ProgressPrinter(int line_number);
	void ReportProgress(float progress);
};

