#include "funcs.h"
#include "settings.h"

void init_settings(Settings& settings, int argc, char* argv[])
{
	// DIRTY HACK
	// command line args should have more priority, so they should be read last
	// but sadly we need to read 'settings-file' from command line first
	for (size_t i = 1; i < argc; i++)
	{
		std::string arg(argv[i]);
		size_t delimiter = arg.find('=');
		if (delimiter == std::string::npos)
			continue;
		std::string cmdline_key = arg.substr(0, delimiter);
		std::string value = arg.substr(delimiter + 1);
		if (Settings::match_settings_file(cmdline_key))
		{
			settings.settings_file = value;
		}
	}

	settings.AddFromFile(settings.settings_file);
	settings.AddFromCommandLine(argc, argv);
}

int main(int argc, char* argv[])
{
	mpreal::set_default_prec(128);
	init_settings(global_settings, argc, argv);
	global_settings.PrintAllSettings();
	auto something = mpfr_buildopt_tls_p();
	unsigned int threads_count = std::thread::hardware_concurrency();
	std::cout.setf(std::ios::fixed); std::cout.precision(20);
	auto start_time = std::chrono::high_resolution_clock::now();


/////////////////////////////////////PARAMS/////////////////////////////////////

	//variable parameters
	int beam_quantity = global_settings.beam_quantity;//(int)pow(10, 4);
	mpreal cell_size = global_settings.cell_size;//pow(10, 0);
	mpreal left_border = global_settings.left_border;//120;
	const int kScatterSize = global_settings.scatter_size;//111;
	mpreal speed = global_settings.speed;//-7100;
	mpreal orbit_height = global_settings.orbit_height; // 1500 * pow(10, 3);
	//some conditions
	mpreal
		wave_length = 532 * pow(10, -9),
		ext_r = "0.1104",//right way to initialize mpreal
		in_r = "0.0639",
		sigma = 16 * pow(10, -4),
		d_i = 5 * pow(10, -3) / beam_quantity;

////////////////////////////////////////////////////////////////////////////////

	//temprary block
	Vector2	sat_speed, final_speed;
	sat_speed.dir.x = speed; sat_speed.dir.y = 0;
	final_speed.dir.x = 0; final_speed.dir.y = 0;
	struct GridParams grid = { {GridParams::Characteristic::Meter}, {0}, {0},
	{0}, {kScatterSize} };
	grid.grid_type = GridParams::Characteristic::Meter;
	grid.center_speed = { {0,0}, {speed,0} };
	grid.cell_size = cell_size;
	grid.left_border = left_border;

/////////////////////////////////OPTICAL SYSTEM/////////////////////////////////

	//tf105
	SchottCoeffs tf105 = { "2.9580175", mpreal("-8.2686725") * pow(10, -3),
		mpreal("39.383391") * pow(10, -3), mpreal("12.219807") * pow(10, -4),
		mpreal("3.1433368") * pow(10, -5), mpreal("86.507903") * pow(10, -7) };

	//k108
	SchottCoeffs k108 = { "2.2699804", mpreal("-9.8250605") * pow(10, -3),
		mpreal("1.1017203") * pow(10, -2), mpreal("7.6606834") * pow(10, -5),
		mpreal("1.1616952") * pow(10, -5), mpreal("5.8130900") * pow(10, -7) };

	//vacuum
	SchottCoeffs vac = { 0,0,0,0,0,0 };

	//external circle coeffs
	SurfCoeffs ext_circle;
	ext_circle.a11 = 1; ext_circle.a22 = 1; ext_circle.a12 = 0;
	ext_circle.a13 = 0; ext_circle.a23 = 0; ext_circle.a33 = -pow(ext_r, 2);

	//inner circle coeffs
	SurfCoeffs inner_circle;
	inner_circle.a11 = 1; inner_circle.a22 = 1; inner_circle.a12 = 0;
	inner_circle.a13 = 0; inner_circle.a23 = 0;
	inner_circle.a33 = -pow(in_r, 2);

	//final plane coeffs
	SurfCoeffs final_coeffs;
	final_coeffs.a11 = 0; final_coeffs.a22 = 0; final_coeffs.a12 = 0;
	final_coeffs.a13 = 0; final_coeffs.a23 = 1;
	final_coeffs.a33 = 2 * orbit_height;

	//Surftypes
	SurfType first_type, second_type, third_type, fourth_type, fifth_type,
		sixth_type, seventh_type, final_type;
	first_type.kind_of_surface = SurfType::Characteristic::Common;
	first_type.schott_coeffs = k108;
	second_type.kind_of_surface = SurfType::Characteristic::Common;
	second_type.schott_coeffs = tf105;
	third_type.kind_of_surface = SurfType::Characteristic::Common;
	third_type.schott_coeffs = k108;
	fourth_type.kind_of_surface = SurfType::Characteristic::Mirror;
	fourth_type.schott_coeffs = k108;
	fifth_type.kind_of_surface = SurfType::Characteristic::Common;
	fifth_type.schott_coeffs = tf105;
	sixth_type.kind_of_surface = SurfType::Characteristic::Common;
	sixth_type.schott_coeffs = k108;
	seventh_type.kind_of_surface = SurfType::Characteristic::Common;
	seventh_type.schott_coeffs = vac;
	final_type.kind_of_surface = SurfType::Characteristic::Ending;

	//Surfaces
	Surface first_surf, second_surf, third_surf, fourth_surf, fifth_surf,
		sixth_surf, seventh_surf, final_surf;
	first_surf.surf_coeffs = ext_circle; first_surf.surf_speed = sat_speed;
	first_surf.surf_type = first_type;
	second_surf.surf_coeffs = inner_circle; second_surf.surf_speed = sat_speed;
	second_surf.surf_type = second_type;
	third_surf.surf_coeffs = inner_circle; third_surf.surf_speed = sat_speed;
	third_surf.surf_type = third_type;
	fourth_surf.surf_coeffs = ext_circle; fourth_surf.surf_speed = sat_speed;
	fourth_surf.surf_type = fourth_type;
	fifth_surf.surf_coeffs = inner_circle; fifth_surf.surf_speed = sat_speed;
	fifth_surf.surf_type = fifth_type;
	sixth_surf.surf_coeffs = inner_circle; sixth_surf.surf_speed = sat_speed;
	sixth_surf.surf_type = sixth_type;
	seventh_surf.surf_coeffs = ext_circle; seventh_surf.surf_speed = sat_speed;
	seventh_surf.surf_type = seventh_type;
	final_surf.surf_coeffs = final_coeffs; final_surf.surf_speed = final_speed;
	final_surf.surf_type = final_type;

////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////BEAMS//////////////////////////////////////

    //beam
	Beam b1;
	b1.w_v.start.x = ext_r * sin(-3 * M_PI / 180); b1.w_v.start.y = -0.1105;
	b1.wave_length = wave_length; b1.amplitude = Gauss(sigma, b1.w_v.start.x, 0);
	b1.w_v.dir.x = 0; b1.w_v.dir.y = 2 * M_PI / b1.wave_length;

	///beamgen
	std::vector<Beam> beams(beam_quantity);
	mpreal startx = ext_r * sin(-3 * M_PI / 180);
	for (int i = 0; i < beam_quantity; i++)
	{
		if(beam_quantity>100)
			if (i % (beam_quantity / 100) == 0)
				std::cout << i * 100 / (beam_quantity) << "%" << "\r";
		b1.w_v.start.x = startx + abs(startx * 2) * i / (beam_quantity - 1);
		b1.amplitude = Gauss(sigma, b1.w_v.start.x, 0);
		beams[i] = b1;
	}
	std::cout << "beamgen done" << std::endl;

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////THINKING////////////////////////////////////

	Modeling(beams, { first_surf, second_surf, third_surf, fourth_surf,
		fifth_surf, sixth_surf, seventh_surf, final_surf }, grid);

////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////DEBUG//////////////////////////////////////

	//place for debug

////////////////////////////////////////////////////////////////////////////////

	//timer stop
	auto end_time = std::chrono::high_resolution_clock::now();
	auto duration = end_time - start_time;
	auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration);
	std::cout << "Evaluation time	" << seconds.count() << std::endl;

	return 0;
}