#pragma once
#define _USE_MATH_DEFINES
#define SPEED_OF_LIGHT 299792458;
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <string>
#include <vector>
#include <array>
#include <tuple>
#include <chrono>
#include <thread>
#include "mpreal.h"
using namespace mpfr;

#if defined(__cpp_lib_filesystem)
    #include <filesystem>
    namespace fs = std::filesystem;
#else
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif

#ifdef _MSC_VER
    #include <windows.h>
    #define USE_WINAPI_CONSOLE
#endif