#pragma once
#include "funcs.h"
#include <functional>
#include <map>

#define PRINTVALUEOF(x) std::cout << #x": " << x << std::endl;
#define SETTING_ITEM(code_identifier, type, default_value, parser) \
private: \
	void update_##code_identifier(const std::string& value) \
 { code_identifier = parser(value); } \
public: \
	inline static const bool match_##code_identifier(const std::string & key) \
	{ return key == #code_identifier; } type code_identifier = Hook<type> \
	(#code_identifier, default_value,\
	std::bind(&Settings::update_##code_identifier, this, std::placeholders::_1));

using UpdaterFunc = std::function<void(const std::string&)>;
using UpdatersMap = std::map<std::string, UpdaterFunc>;

class Settings
{
private:
	// This map MUST be declared first
	// because SETTING_ITEM macro WANTS THEM INITIALIZED FIRST
	// and in C++ order initialization depends on declaration order
	UpdatersMap updaters_map;
	SETTING_ITEM(beam_quantity, size_t, 10000, ParseLong)
	SETTING_ITEM(speed, mpreal, 0, ParseMpreal)
	SETTING_ITEM(orbit_height, mpreal, 15, ParseMpreal)
	SETTING_ITEM(cell_size, mpreal, pow(10,-5), ParseMpreal)
	SETTING_ITEM(left_border, mpreal, -pow(10,-2), ParseMpreal)
	SETTING_ITEM(scatter_size, size_t, 2001, ParseLong)
	SETTING_ITEM(settings_file, std::string, "settings.txt",
		[](const std::string& in) { return in; })
	SETTING_ITEM(show_progress, bool, true, ParseBool)
	SETTING_ITEM(threads, int, 0, ParseInt)
public:	
	Settings();
	void AddFromFile(const std::string& path);
	void AddFromCommandLine(int argc, char* argv[]);
	void PrintAllSettings();
private:
	void Add(const std::string& key, const std::string value);

	static size_t ParseLong(const std::string& value);
	static int ParseInt(const std::string& value);
	static bool ParseBool(const std::string& value);
	static mpreal ParseMpreal(const std::string& value);

	template<typename T>
	T Hook(const std::string& full_name, T default_value, UpdaterFunc);	
};

extern Settings global_settings;
