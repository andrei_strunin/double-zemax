#!/bin/bash

# Make sure we in solution folder
if test -f "doublezemax/main.cpp"; then
    mkdir -p "x64/linux/"
    cp doublezemax/settings.txt x64/linux/
    g++ -O2 -std=c++17 -Ithird-party/mpreal \
        doublezemax/main.cpp doublezemax/Funcs.cpp doublezemax/ProgressPrinter.cpp doublezemax/Settings.cpp \
        -lmpfr -lgmp -lpthread -lstdc++fs -o x64/linux/doublezemax
else
    echo "build.sh must be called from solution root folder!"
fi
