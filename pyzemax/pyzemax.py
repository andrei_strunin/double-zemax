from doublezemax_caller import start_doublezemax
from settings import Settings

settings_set = [
    Settings(12345, 0, 15, 10 ** -5, -10 ** -2, 1991, 6, True),
    Settings(beam_quantity=10000,
             speed=0,
             orbit_height=15,
             cell_size=10 ** -5,
             left_border=-10 ** -2,
             scatter_size=2001)
]

for set in settings_set:
    start_doublezemax(set)
