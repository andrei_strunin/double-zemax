import subprocess
import platform
from settings import Settings

if platform.system() == 'Windows':
    dll_path = "../x64/Release/doublezemax.exe"
else:
    dll_path = "../x64/linux/doublezemax"

def start_doublezemax(settings: Settings):
    subprocess.call([dll_path] + settings.get_attr_array())
