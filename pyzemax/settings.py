class Settings:
    def __init__(self,
                 beam_quantity: int,
                 speed: float,
                 orbit_height: float,
                 cell_size: float,
                 left_border: float,
                 scatter_size: int,
                 threads: int = 0,
                 show_progress: bool = True):
        # class fields MUST have same names as in Settings.h of doublezemax
        self.beam_quantity = beam_quantity
        self.speed = speed
        self.orbit_height = orbit_height
        self.cell_size = cell_size
        self.left_border = left_border
        self.scatter_size = scatter_size
        self.show_progress = show_progress
        self.threads = threads

    def get_attr_array(self):
        return [a + '=' + str(getattr(self, a)) for a in dir(self) if not a.startswith('__') and not callable(getattr(self, a))]
